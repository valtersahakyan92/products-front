import {createRouter, createWebHistory} from "vue-router";

const routes = [
    {
        path: "/",
        name: "initial",
        component: () => import("@/pages/Home"),
        children:[]
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;