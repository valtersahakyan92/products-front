const products = [];
for (let i = 1; i <= 10; i++) {
    products.push({
        id: i,
        title: `title ${i}`,
        description: `description ${i}`,
        price: i.toFixed(2),
        image: 'https://cdn.shopify.com/s/files/1/0627/9164/7477/products/pocket-mirror-01_2000x.jpg?v=1660936331',
    })
}
export default products;