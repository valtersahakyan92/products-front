import Vuex from 'vuex';
import products from "@/constants/products";

export const store = new Vuex.Store({
    state: {
        products: [],
        myProducts: []
    },
    mutations: {
        setMyProducts(state, data){
            state.myProducts = data.products;
        },
        setProducts(state, data){
            state.products = data.products;
        },
    },
    actions: {
        addMyProducts({ commit, state },data) {
            const products = JSON.parse(localStorage.getItem('products')) || [];
            const product = state.products.find(item => item.id === data.id);
            const newProducts = [...products, product.id ];
            localStorage.setItem('products', JSON.stringify(newProducts));
            commit('setMyProducts', {products: newProducts});
        },
        removeMyProducts({ commit }, data){
            const products = JSON.parse(localStorage.getItem('products')) || [];
            const newProducts = products.filter(product => product !== data.id);
            localStorage.setItem('products', JSON.stringify(newProducts));
            commit('setMyProducts', {products: newProducts});
        },
        setAllProducts({ commit }){
            commit('setProducts', {products: products});
        },
        setMyProducts({ commit }){
            commit('setMyProducts', {products: JSON.parse(localStorage.getItem('products')) || []});
        }
    },
    getters: {
        getProducts (state) {
            return state.products;
        },
        getMyProducts (state) {
            return state.myProducts;
        },
    },
});